# Introduction to bloom-filter

##The Bloom Filter object##
Parameters:

* `bits`: it's the physical filter, a list of bit stored as an array of 128 bits per cell
* `m`: the size of the seri you want to store in the filter
* `n`: how many bits has the filter.
* `p`: false positive rate
* `k`: number of hash functions

##Main functions overview##

###create###

* `(create m p)`
* `(create m n 'p)`: `p` MUST NOT be a number or `nil`. This method is meant for the situation in which you don't need to force a value on p so the optimal one is evaluated. 

both the create functions returns a blank Bloom Filter object. 
Usage example might be:

* `(def bf (create 10000 0.00001))` 
* `(def bf (create 10000 50000 'p))` 

you cannot force both the size of the filter and the false positive rate because otherwise you won't obtain a Bloom Filter. I know that writing 'p instead of the probability value might sound strange at the beginning but this function will fail more easily if your code has some errors.

###insert###
You can add elements in a Bloom Filter with two different functions:

*  `(insert a bloom)` where `a` is an element and `bloom` a Bloom Filter. Return a new bloom filter with `a` as additional element.
* `(insert-vec vec bloom)` where `vec` is a vector of elements: every element in this vector will be stored in the filter.

 Example: `(def bf (insert "foo" bf))` where bf is an existing Bloom Filter.
 
###query###
To look for an element `a` in the Bloom Filter `bloom` you can use `(find-element a bloom)`. It returns a boolean value.
 