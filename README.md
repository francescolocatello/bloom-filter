#bloom-filter#

If you stumbled into this Clojure library you might know what a Bloom Filter is. If you don't, you have just found a solution to store effectively your data sets in a concurrent scenario. Memory occupation in a parallel and concurrent architecture might have been your major problems but with this library things are gonna change!

Bloom filter (wikipedia): 
> "A Bloom filter is a space-efficient probabilistic data structure, conceived by Burton Howard Bloom in  
> 1970, that is used to test whether an element is a member of a set. False positive matches are
> possible, but false negatives are not, thus a Bloom filter has a 100% recall rate".

Bloom filter principle:
> Wherever a list or set is used, and space is at premium consider using a Bloom filter if the effect of 
> false positives can be mitigated.

### What can I store in a Bloom Filter? ###

Well, you can store everything can be passed as a parameter of the hash function. Just keep in mind the simple rules of Bloom Filters:

* you can store as many elements you want in a filter
* you can quest the structure for membership and there are two possible answers: the element is NOT in the set or the element MIGHT be in the set with a certain probability
* you can decide that probability
* you can decide how big you want to do the filter but the more elements you store, the higher the false positive rate must be: there are some mathematical relations that must be followed

####Do I really have to know all this stuff? ####
No. 

All you have to know is:

* how many elements does your data set have
* how much space do you want to use or which is the false positive rate do you want to reach

Tip:

If you just know how many elements does your dataset have (lets say they are m) and you want to build a great bloom filter with an almost zero false positive rate you can calculate the optimal space as `m*log_2 (m)`

###Why Clojure??###
There are many reasons to use this implementation in your projects:

* Clojure is a Lisp dialect
* Writing code in Clojure is faster than in Java
* Why a comparison with java? Because Clojure runs on JVM
* Clojure has a transactional memory. Here we are not dealing with a simple Bloom Filter but with a persistent one!
* Clojure is pretty fast too. Here you have the link of the benchmarks to compare the performance of this implementation with a non persistent Java one. [SPOILER] Java is less then an order of magnitude faster than Clojure: in less than a blink your Bloom Filter is persistent!

### How do I get set up? ###
You can add the library manually adding this library as a dependency to `project.clj`:
`[bloom-filter "1.0.0"]`

###Quick summary###
how to use functions: 

 * `(def bf (create m p))` 
 * `(def bf (create m n 'p))`
 * `(def bf (insert "foo" bf))`
 * `(def bf (insert-vec vector bf))`
 * `(find-element "foo" bf)`

these are just some examples. Take a look at the documentation to understand how to use these functions.

### How to run tests###
* `lein test`

there are two tests: the first one checks whether all the elements you store in your filter are actually founded and the second one checks that the experimental false positive rate matches the theoretical expectations.

If you look at the test's code you can see that there are two commented lines: if you uncomment them you will make the test faster (but less accurate).

### Contribution guidelines ###

* Writing tests
* Code review

I invite you to improve freely this library and contact me whether you need informations on this library.

### Who do I talk to? ###

francesco.locatello@gmail.com

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
