(ns bloom-filter.core-test
  (:require [clojure.test :refer :all]
            [bloom-filter.core :refer :all]))

(use 'clojure.java.io)

(def txt (clojure.string/split
           (str
             (slurp "test/bloom_filter/web2")) #"\n+"))
;(def txt (subvec txt (-(count txt) 100)))
(def bf (create (count txt) 0.001))
(def bf (insert-vec txt bf))

(deftest found-all
  "check whether all the elements of the set web2 stored in the filter are correctly founded"
  (is
    (= true
       (reduce
         (fn [x y] (and x y))
         (for [k txt] (find-element k bf))))))
(def txt2 (clojure.string/split (str (slurp "test/bloom_filter/web2a")) #"\n+"))
;(def txt2 (subvec txt2 (-(count txt2) 100)))
(def result (for [k txt2] (find-element k bf)))
(def rate (/
            (count
              (filter (fn [x] (= x true)) result))
            (count result)))
rate
(+ (.p bf) (* 0.0001 (.p bf)))
(> (+ (.p bf) (* 0.0001 (.p bf))) rate)
(< (+ (.p bf) (* 0.0001 (.p bf))) rate)


(deftest false-positive
  "check whether the false positive rate matches the theoretical expectations"
  (is
    (and
      (> (+ (.p bf) (* 0.0001 (.p bf))) rate))))

